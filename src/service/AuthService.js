import axios from "axios";
import { API_URL, LOGIN_URL } from ".././config/setting.js";
const api = axios.create({
    baseURL: API_URL
})

class AuthService  {

    async test(){
        console.log("test");
    }

    async register(data){
        let res = await api.post('signup', data, {
            headers: {
              'Content-Type': 'application/json',
              "Access-Control-Allow-Origin": "*"
            }
        });

        return res;
    }

    async login(data){

        var params = new URLSearchParams();
        params.append('username', data.username)
        params.append('password', data.password)
        params.append('grant_type', data.grant_type)
        params.append('client_id', data.client_id)
        params.append('client_secret', data.client_secret)
        params.append('scope', data.scope);

        var login = axios.create({
            baseURL: LOGIN_URL
        })
    
        let res = await login.post('', params, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;',
            "Access-Control-Allow-Origin": "*"
          }
        });

        return res;
    }

    async checkToken(){
        const token = localStorage.getItem('token');
        if(!token){
            window.location.href = "?#/login";
        }
    }

    async logoutUser(){
        localStorage.removeItem('token');
        localStorage.removeItem('role');
        window.location.href = "?#/login";
    }
  
}

export default new AuthService();