

class ApiService{
    constructor(){
        
    }

    httpHeaders(){
        const accessToken = localStorage.getItem('token');
        return {
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*",
            'Authorization': 'Bearer ' + accessToken
        }
    }

    
}

export default new ApiService();