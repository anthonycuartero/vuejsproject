import axios from "axios";
import { API_URL } from ".././config/setting.js";
import ApiService from "./ApiService.js";
import AuthService from "./AuthService.js";
const api = axios.create({
    baseURL: API_URL
})

class SubjectService {
    constructor() {
        api.interceptors.response.use(
            res => {
                return res;
            },
            err => {
                if (err.response.status == 401) {
                    AuthService.logoutUser();
                }
            }
        );
    }

    async getSubjectList(keyword) {
        let params = new URLSearchParams();
        params.append('Keyword', keyword);

        return await api.get('subjects', { headers: ApiService.httpHeaders(), params: params });
    }

    async getSubjectDetails(id) {
        return await api.get(`subjects/${id}`, { headers: ApiService.httpHeaders() });
    }

    async insertSubject(subject) {
        return await api.post(`subjects`, subject, { headers: ApiService.httpHeaders() });
    }

    async updateSubject(subject) {
        var id = subject.id;
        return await api.patch(`subjects/${id}`, subject, { headers: ApiService.httpHeaders() });
    }

    async deleteSubject(id) {
        return await api.delete(`subjects/${id}`, { headers: ApiService.httpHeaders() });
    }

}

export default new SubjectService();