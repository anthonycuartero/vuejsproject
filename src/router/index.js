import { createRouter, createWebHashHistory } from "vue-router";

import DashboardLayout from "@/layout/DashboardLayout";
import AuthLayout from "@/layout/AuthLayout";

import Dashboard from "../views/Dashboard.vue";
import Icons from "../views/Icons.vue";
import Maps from "../views/Maps.vue";
import Profile from "../views/UserProfile.vue";
import Tables from "../views/Tables.vue";
import Course from "../views/Course.vue";
import Module from "../views/Module.vue";
import Content from "../views/Content.vue";
import StudentCourse from "../views/StudentCourse.vue";
import StudentModule from "../views/StudentModule.vue";

import Login from "../views/Login.vue";
import Register from "../views/Register.vue";

const routes = [
  {
    path: "/",
    redirect: "/dashboard",
    component: DashboardLayout,
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        components: { default: Dashboard },
      },
      {
        path: "/course/:subjectId",
        name: "course",
        components: { default: Course },
      },
      {
        path: "/module/:subjectId/:courseId",
        name: "module",
        components: { default: Module },
      },
      {
        path: "/studentcourse",
        name: "studentcourse",
        components: { default: StudentCourse },
      },
      {
        path: "/studentmodule/:courseId",
        name: "studentmodule",
        components: { default: StudentModule },
      },
      {
        path: "/content/:subjectId/:courseId/:moduleId",
        name: "content",
        components: { default: Content },
      },
      {
        path: "/icons",
        name: "icons",
        components: { default: Icons },
      },
      {
        path: "/maps",
        name: "maps",
        components: { default: Maps },
      },
      {
        path: "/profile",
        name: "profile",
        components: { default: Profile },
      },
      {
        path: "/tables",
        name: "tables",
        components: { default: Tables },
      },
    ],
  },
  {
    path: "/",
    redirect: "login",
    component: AuthLayout,
    children: [
      {
        path: "/login",
        name: "login",
        components: { default: Login },
      },
      {
        path: "/register",
        name: "register",
        components: { default: Register },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  linkActiveClass: "active",
  routes,
});

export default router;
